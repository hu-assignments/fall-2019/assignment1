OBJS	= src/core/main.o src/lib/str/strutils.o src/lib/fileio/fileutils.o src/core/matrixman.o src/lib/types/linkedlist.o src/core/command_executor.o src/lib/math/matrix.o src/lib/math/mathutils.o
SOURCE	= src/core/main.c src/lib/str/strutils.c src/lib/fileio/fileutils.c src/core/matrixman.c src/lib/types/linkedlist.c src/core/command_executor.c src/lib/math/matrix.c src/lib/math/mathutils.c
HEADER	= src/core/main.h src/lib/str/strutils.h src/lib/memory/memutils.h src/lib/fileio/fileutils.h src/lib/math/mathutils.h src/core/matrixman.h src/lib/types/linkedlist.h src/core/command_executor.h src/lib/math/matrix.h
OUT	= matrixman
CC	= gcc
LFLAGS	 = -lm

all: matrixman clean_obj_files

.PHONY: matrixman
matrixman: $(OBJS)
	@echo "[INFO] Linking C executable matrixman..."
	@$(CC)  $^ $(LFLAGS) -o $@
	@echo "[OK]   Built C executable matrixman"

%.o: %.c $(HEADER)
	@echo "[INFO] Compiling and assembling $<"
	@$(CC) -c -o $@ $< $(LFLAGS)

.PHONY: clean_obj_files
clean_obj_files:
	@echo "[OK]   Cleaning object files..."
	@rm -f $(OBJS)
	@echo "[OK]   Object files are cleaned."

.PHONY: clean
clean:
	@echo "[INFO] Cleaning project..."
	@rm -f $(OBJS) $(OUT)
	@echo "[OK]   Project cleaned."