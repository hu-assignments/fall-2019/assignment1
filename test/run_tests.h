/**
 * Created on 01/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef ASSIGNMENT1_TEST_RUN_TESTS_H
#define ASSIGNMENT1_TEST_RUN_TESTS_H

#include <stdbool.h>

bool
test_all();

#endif //ASSIGNMENT1_TEST_RUN_TESTS_H
