/**
 * Created on 01/11/2019.
 * @author M. Samil Atesoglu
 */

#include <stdlib.h>
#include <stdio.h>

#include "run_tests.h"
#include "lib/types/unit_test_list.h"
#include "core/unit_test_matrixman.h"
#include "lib/math/unit_test_matrix.h"

int
main(int argc, char *argv[])
{
    bool successful = test_all();
    if (!successful)
    {
        fprintf(stderr, "%-6s Unit tests have failed.\n", "[FAIL]");
        exit(EXIT_FAILURE);
    } else printf("%-6s All unit tests have passed.\n", "[OK]");
    return EXIT_SUCCESS;
}

bool
test_all()
{
    printf("%-6s Testing all unit tests...\n", "[INFO]");

    bool ok = true;

    test_linked_list(&ok);
    test_matrixman(&ok);
    test_math_matrix(&ok);

    return ok;
}