/**
 * Created on 10/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef ASSIGNMENT1_TEST_UNIT_TEST_MATH_UTILS_H
#define ASSIGNMENT1_TEST_UNIT_TEST_MATH_UTILS_H

#include <stdbool.h>

void
test_math_matrix(bool *ok);

bool
test_matrix_concat_d(bool *ok);

bool
test_matrix_concat_r(bool *ok);

bool
test_matrix_copy(bool *ok);

bool
test_matrix_transpose(bool *ok);

bool
test_matrix_slice(bool *ok);

bool
test_matrix_enlarge(bool *ok);

#endif //ASSIGNMENT1_TEST_UNIT_TEST_MATH_UTILS_H
