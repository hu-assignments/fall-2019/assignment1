/**
 * Created on 10/11/2019.
 * @author M. Samil Atesoglu
 */

#include "unit_test_matrix.h"
#include "../../../src/lib/math/mathutils.h"
#include "../../../src/core/matrixman.h"
#include "../../../src/lib/str/strutils.h"
#include <stdio.h>

void
test_math_matrix(bool *ok)
{
    printf("%-6s Testing math_utils...\n", "[TEST]");

    printf("%-6s test_matrix_concat_d\n", test_matrix_concat_d(ok) ? "[OK]" : "[FAIL]");
    printf("%-6s test_matrix_concat_r\n", test_matrix_concat_r(ok) ? "[OK]" : "[FAIL]");
    printf("%-6s test_matrix_copy\n", test_matrix_copy(ok) ? "[OK]" : "[FAIL]");
    printf("%-6s test_matrix_transpose\n", test_matrix_transpose(ok) ? "[OK]" : "[FAIL]");
    printf("%-6s test_matrix_slice\n", test_matrix_slice(ok) ? "[OK]" : "[FAIL]");
    printf("%-6s test_matrix_enlarge\n", test_matrix_enlarge(ok) ? "[OK]" : "[FAIL]");
}

bool
test_matrix_concat_d(bool *ok)
{
    char *str_m1 = strdup("1 1 1\n2 2 2\n3 3 3\n");
    char *str_m2 = strdup("4 4 4\n5 5 5\n6 6 6\n");

    Matrix *m1 = str_to_matrix(str_m1);
    Matrix *m2 = str_to_matrix(str_m2);

    matrix_concat(m1, m2, MATRIX_CONCAT_MODE_DOWN);

    char *result_str = matrix_to_str(*m1);

    bool passed = EQUALS(result_str, "1 1 1\n2 2 2\n3 3 3\n4 4 4\n5 5 5\n6 6 6\n");

    if (*ok == true)
        *ok = passed;

    free(str_m1);
    free(str_m2);
    free(result_str);

    free_matrix(m1);
    free_matrix(m2);

    return passed;
}

bool
test_matrix_concat_r(bool *ok)
{
    char *str_m1 = strdup("1 1 1\n2 2 2\n3 3 3\n");
    char *str_m2 = strdup("4 4\n5 5\n6 6\n");

    Matrix *m1 = str_to_matrix(str_m1);
    Matrix *m2 = str_to_matrix(str_m2);

    matrix_concat(m1, m2, MATRIX_CONCAT_MODE_RIGHT);

    char *result_str = matrix_to_str(*m1);

    bool passed = EQUALS(result_str, "1 1 1 4 4\n2 2 2 5 5\n3 3 3 6 6\n");

    if (*ok == true)
        *ok = passed;

    free(str_m1);
    free(str_m2);
    free(result_str);

    free_matrix(m1);
    free_matrix(m2);

    if (*ok == true)
        *ok = passed;

    return *ok;
}

bool
test_matrix_copy(bool *ok)
{
    char *str_m1 = strdup("1 1 1\n2 2 2\n");

    Matrix *m1 = str_to_matrix(str_m1);
    Matrix *m2 = matrix_copy(*m1);

    char *result_str = matrix_to_str(*m2);

    bool passed = EQUALS(result_str, "1 1 1\n2 2 2\n");

    if (*ok == true)
        *ok = passed;

    free(result_str);
    free(str_m1);
    free_matrix(m1);
    free_matrix(m2);

    return passed;
}

bool
test_matrix_transpose(bool *ok)
{
    char *str_m1 = strdup("1 1 1\n2 2 2\n");

    Matrix *m1 = str_to_matrix(str_m1);
    matrix_transpose(m1);

    char *result_str = matrix_to_str(*m1);

    bool passed = EQUALS(result_str, "1 2\n1 2\n1 2\n");

    if (*ok == true)
        *ok = passed;

    free(result_str);
    free(str_m1);
    free_matrix(m1);

    return passed;
}

bool
test_matrix_slice(bool *ok)
{
    char *str_m1 = strdup("1 2 3\n4 5 6\n7 8 9\n0 -1 -2\n");

    Matrix *m1 = str_to_matrix(str_m1);

    Matrix *m2 = matrix_slice(*m1, 1, 2, 1, 2);

    char *result_str = matrix_to_str(*m2);

    bool passed = EQUALS(result_str, "5 6\n8 9\n");

    if (*ok == true)
        *ok = passed;

    free_matrix(m1);
    free_matrix(m2);
    free(str_m1);
    free(result_str);

    return passed;
}

bool
test_matrix_enlarge(bool *ok)
{
    char *str_matrix = strdup("1 2 3\n4 5 6\n7 8 9\n0 -1 -2\n");

    Matrix *m = str_to_matrix(str_matrix);
    
    matrix_enlarge(m, 3, 2, 9);

    char *result_str = matrix_to_str(*m);

    bool passed = EQUALS(result_str, "1 2 3 9 9\n4 5 6 9 9\n7 8 9 9 9\n0 -1 -2 9 9\n9 9 9 9 9\n9 9 9 9 9\n9 9 9 9 9\n");

    if (*ok == true)
        *ok = passed;

    free_matrix(m); 
    free(str_matrix);
    free(result_str);

    return passed;
    
}