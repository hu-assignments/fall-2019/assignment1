
/**
 * Created on 30/10/2019.
 * @author M. Samil Atesoglu
 */

#include <stdio.h>
#include "unit_test_list.h"
#include "../../../src/lib/types/linkedlist.h"
#include "../../../src/lib/str/strutils.h"

void
test_linked_list(bool *ok)
{
    printf("%-6s Testing linked_list...\n", "[TEST]");

    printf("%-6s test_list_length_is_five_when_there_are_five_elements\n",
           test_list_length_is_five_when_there_are_five_elements(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_list_length_is_one_when_there_is_one_element\n",
           test_list_length_is_one_when_there_is_one_element(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_list_length_is_zero_when_empty\n", test_list_length_is_zero_when_empty(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_free_list\n", test_free_list(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_list_get_index_returns_correct_value\n", test_list_get_index_returns_correct_value(ok) ? "[OK]" : "[FAIL]");
}

bool
test_list_get_index_returns_correct_value(bool *ok)
{
    LinkedList string_list;
    list_construct(&string_list, NULL);

    list_push(&string_list, "1abcdsfadfdfdsf");
    list_push(&string_list, "2csadade");
    list_push(&string_list, "3fadsasdsgh");
    list_push(&string_list, "4ijasdk");
    list_push(&string_list, "5gkds");

    char *str = LIST_GET(&string_list, 1, char *);

    bool passed = EQUALS(str, "2csadade");
    if (*ok == true)
        *ok = passed;

    list_free(&string_list);

    return passed;
}

bool
test_list_length_is_five_when_there_are_five_elements(bool *ok)
{
    LinkedList string_list;
    list_construct(&string_list,  NULL);

    list_push(&string_list, "1abcdsfadfdfdsf");
    list_push(&string_list, "2csadade");
    list_push(&string_list, "3fadsasdsgh");
    list_push(&string_list, "4ijasdk");
    list_push(&string_list, "5gkds");

    bool passed = list_length(&string_list) == 5;
    if (*ok == true)
        *ok = passed;

    list_free(&string_list);

    return passed;
}

bool
test_list_length_is_one_when_there_is_one_element(bool *ok)
{
    LinkedList string_list;
    list_construct(&string_list, NULL);

    list_push(&string_list, "1abcdsfadfdfdsf");

    bool passed = list_length(&string_list) == 1;
    if (*ok == true)
        *ok = passed;

    list_free(&string_list);

    return passed;
}

bool
test_list_length_is_zero_when_empty(bool *ok)
{
    LinkedList string_list;
    list_construct(&string_list, NULL);

    list_push(&string_list, "1abcdsfadfdfdsf");
    list_push(&string_list, "2csadade");
    list_push(&string_list, "3fadsasdsgh");

    list_pop(&string_list);
    list_pop(&string_list);
    list_pop(&string_list);

    bool passed = list_length(&string_list) == 0;
    if (*ok == true)
        *ok = passed;

    list_free(&string_list);

    return passed;
}

bool
test_free_list(bool *ok)
{
    LinkedList string_list;
    list_construct(&string_list, &free_element);

    list_push(&string_list, strdup("1first"));
    list_push(&string_list, strdup("2second"));

    list_free(&string_list);

    bool passed = string_list.head == NULL;
    if (*ok == true)
        *ok = passed;

    return passed;
}

void
free_element(void *data)
{
    free(data);
}