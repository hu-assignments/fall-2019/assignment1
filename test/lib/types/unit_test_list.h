/**
 * Created on 30/10/2019.
 * @author M. Samil Atesoglu
 */

#ifndef ASSIGNMENT1_UNIT_TEST_LIST_H
#define ASSIGNMENT1_UNIT_TEST_LIST_H

#include <stdbool.h>
#include <string.h>

void
free_element(void *data);

void
test_linked_list(bool *ok);

bool
test_list_length_is_five_when_there_are_five_elements(bool *ok);

bool
test_list_length_is_one_when_there_is_one_element(bool *ok);

bool
test_list_length_is_zero_when_empty(bool *ok);

bool
test_free_list(bool *ok);

bool
test_list_get_index_returns_correct_value(bool *ok);

#endif //ASSIGNMENT1_UNIT_TEST_LIST_H
