/**
 * Created on 06/11/2019.
 * @author M. Samil Atesoglu
 */

#include <stdio.h>
#include "unit_test_matrixman.h"
#include "../../src/lib/memory/memutils.h"
#include "../../src/lib/str/strutils.h"
#include "../../src/core/matrixman.h"

void
test_matrixman(bool *ok)
{
    printf("%-6s Testing matrixman functions...\n", "[TEST]");

    printf("%-6s test_get_named_matrix_by_name\n", test_get_named_matrix_by_name(ok) ? "[OK]" : "[FAIL]");
}

bool
test_get_named_matrix_by_name(bool *ok)
{
    LinkedList matrix_list;
    list_construct(&matrix_list, NULL);

    NamedMatrix m1;
    NamedMatrix m2;
    NamedMatrix m3;

    m1.name = "name1";
    m2.name = "name2";
    m3.name = "name3";

    list_push(&matrix_list, &m1);
    list_push(&matrix_list, &m2);
    list_push(&matrix_list, &m3);

    bool passed = EQUALS(get_named_matrix_by_name(&matrix_list, "name1")->name, "name1");
    if (*ok == true)
        *ok = passed;

    list_free(&matrix_list);

    return passed;
}