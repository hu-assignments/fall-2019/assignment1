/**
 * Created on 06/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef ASSIGNMENT1_TEST_UNIT_TEST_MATRIXMAN_H
#define ASSIGNMENT1_TEST_UNIT_TEST_MATRIXMAN_H

#include <stdbool.h>

void
test_matrixman(bool *ok);

bool
test_get_named_matrix_by_name(bool *ok);

#endif //ASSIGNMENT1_TEST_UNIT_TEST_MATRIXMAN_H
