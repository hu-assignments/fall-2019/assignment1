/**
 * Created on 13/10/2019.
 * @author M. Samil Atesoglu
 */

#ifndef MEM_UTILS_H
#define MEM_UTILS_H

#include <stdlib.h>

#define ALLOC(count, type) (type *) calloc(count, sizeof(type))
#define REALLOC(ptr, new_size, type) (type *) realloc(ptr, (new_size) * sizeof(type))

#endif //MEM_UTILS_H
