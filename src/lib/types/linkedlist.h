/**
 * Created on 27/10/2019.
 * @author M. Samil Atesoglu
 */

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stddef.h>

#define LIST_GET(list, index, type) ((type) list_get_by_index(list, index))

struct Node
{
    void *data;
    struct Node *next;
};

typedef struct Node Node;

typedef struct
{
    Node *head;
    void (*free_func_ptr)(void *);
} LinkedList;

void
list_push(LinkedList *list, void *data);

void *
list_pop(LinkedList *list);

unsigned int
list_length(LinkedList *list);

void
list_free(LinkedList *list);

void
list_append(LinkedList *list, void *data);

void *
list_get_by_index(LinkedList *list, unsigned int index);

void
list_construct(LinkedList *list, void (*free_func_ptr)(void *));

#endif //LINKEDLIST_H
