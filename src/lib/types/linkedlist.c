/**
 * Created on 27/10/2019.
 * @author M. Samil Atesoglu
 */

#include "linkedlist.h"
#include "../memory/memutils.h"

void
list_push(LinkedList *list, void *data)
{
    if (list->head == NULL)
    {
        Node *head_node = ALLOC(1, Node);
        head_node->data = data;
        list->head = head_node;
    }
    else
    {
        Node **head = &(list->head);
        Node *new_node = ALLOC(1, Node);
        new_node->data = data;
        new_node->next = *head;
        *head = new_node;
    }
}

unsigned int
list_length(LinkedList *list)
{
    Node *current = list->head;

    unsigned int length = 0;
    while (current != NULL)
    {
        length++;
        current = current->next;
    }

    return length;
}

void *
list_pop(LinkedList *list)
{
    Node **head = &(list->head);

    void *return_value;
    Node *next_node = NULL;

    if (*head == NULL)
    {
        return NULL;
    }

    next_node = (*head)->next;
    return_value = (*head)->data;
    free(*head);
    *head = next_node;

    return return_value;
}

void
list_free(LinkedList *list)
{
    Node *current;
    while (list->head != NULL)
    {
        current = list->head;
        list->head = current->next;

        if (list->free_func_ptr)
        {
            list->free_func_ptr(current->data);
        }

        free(current);
    }
}

void
list_construct(LinkedList *list, void (*free_func_ptr)(void *))
{
    list->head = NULL;
    list->free_func_ptr = free_func_ptr;
}

void *
list_get_by_index(LinkedList *list, unsigned int index)
{
    index = list_length(list) - index - 1;
    Node *current = list->head;

    unsigned int current_index = 0;
    while (current != NULL)
    {
        if (current_index == index)
            return current->data;
        current_index++;
        current = current->next;
    }
    return NULL;
}