/**
 * Created on 12/11/2019.
 * @author M. Samil Atesoglu
 */

#include "matrix.h"
#include "../memory/memutils.h"
#include "mathutils.h"

Matrix *
form_zero_matrix(unsigned int row_count, unsigned int col_count)
{
    return form_matrix(row_count, col_count, 0);
}

void
matrix_fill(Matrix *matrix, int val)
{
    unsigned int i, j;
    for (i = 0; i < matrix->row_count; i++)
        for (j = 0; j < matrix->col_count; j++)
            matrix->grid[i][j] = val;
}

Matrix *
form_matrix(unsigned int row_count, unsigned int col_count, int val)
{
    Matrix *matrix = ALLOC(1, Matrix);

    matrix->row_count = row_count;
    matrix->col_count = col_count;

    matrix->grid = ALLOC(row_count, int*);

    unsigned int i;
    for (i = 0; i < row_count; i++)
    {
        matrix->grid[i] = ALLOC(col_count, int);
    }

    matrix_fill(matrix, val);

    return matrix;
}

void
matrix_enlarge(Matrix *matrix, unsigned int enlarge_factor_r, unsigned int enlarge_factor_c, int val)
{
    unsigned int old_row_count = matrix->row_count, old_col_count = matrix->col_count;
    unsigned int enlarged_row_count = old_row_count + enlarge_factor_r;
    unsigned int enlarged_col_count = old_col_count + enlarge_factor_c;

    matrix->grid = REALLOC(matrix->grid, enlarged_row_count, int *);

    unsigned int i, j;
    for (i = 0; i < enlarged_row_count; i++)
    {
        if (i < old_row_count)
            matrix->grid[i] = REALLOC(matrix->grid[i], enlarged_col_count, int);
        else
            matrix->grid[i] = ALLOC(enlarged_col_count, int);
    }

    for (i = 0; i < enlarged_row_count; i++)
    {
        for (j = 0; j < enlarged_col_count; j++)
        {
            if (i >= old_row_count || j >= old_col_count)
                matrix->grid[i][j] = val;
        }
    }

    matrix->row_count = enlarged_row_count;
    matrix->col_count = enlarged_col_count;
}

void
matrix_perform_element_wise_operation(Matrix *m1, Matrix *m2, char operation)
{
    unsigned int i;
    for (i = 0; i < m1->row_count; i++)
    {
        unsigned int j;
        for (j = 0; j < m1->col_count; j++)
        {
            switch (operation)
            {
                case MATH_ADD:
                    m1->grid[i][j] += m2->grid[i][j];
                    break;
                case MATH_SUBTRACT:
                    m1->grid[i][j] -= m2->grid[i][j];
                    break;
                case MATH_MULTIPLY:
                    m1->grid[i][j] *= m2->grid[i][j];
                    break;
                default:
                    break;
            }
        }
    }
}

void
matrix_transpose(Matrix *matrix)
{
    unsigned int old_row_count = matrix->row_count;
    unsigned int old_col_count = matrix->col_count;

    unsigned int rc_temp = matrix->row_count;
    matrix->row_count = matrix->col_count;
    matrix->col_count = rc_temp;

    unsigned int room = (unsigned int) fmax(old_row_count, old_col_count);

    // Check if the grid is not square.
    if (old_col_count != old_row_count)
    {
        // Allocating enough space for swap operation.
        matrix->grid = REALLOC(matrix->grid, room, int *);

        unsigned int i;
        for (i = 0; i < old_row_count; i++)
            matrix->grid[i] = REALLOC(matrix->grid[i], room, int);

        for (; i < room; i++)
            matrix->grid[i] = ALLOC(room, int);
    }
    // The allocated space for the matrix is shaped like a square at this point.


    // Begin transposition.
    int temp;
    unsigned int i;
    for (i = 0; i < room; i++)
    {
        unsigned int j;
        for (j = i; j < room; j++)
        {
            temp = matrix->grid[i][j];
            matrix->grid[i][j] = matrix->grid[j][i];
            matrix->grid[j][i] = temp;
        }
    }

    // Shrink the memory for the matrix.
    for (i = 0; i < matrix->row_count; i++)
        matrix->grid[i] = REALLOC(matrix->grid[i], matrix->col_count, int);

    // Free the empty rows after transposition.
    for (; i < room; i++)
        free(matrix->grid[i]);

    // Shrink the rows of matrix with the new row count.
    matrix->grid = REALLOC(matrix->grid, matrix->row_count, int *);
}

void
matrix_concat(Matrix *m1, Matrix *m2, int mode)
{
    if (mode == MATRIX_CONCAT_MODE_DOWN)
    {
        m1->grid = REALLOC(m1->grid, m1->row_count + m2->row_count, int *);
        unsigned int start_index = m1->row_count;
        m1->row_count += m2->row_count;
        unsigned int i;
        for (i = start_index; i < m1->row_count; i++)
        {
            m1->grid[i] = ALLOC(m1->col_count, int);
            unsigned int j;
            for (j = 0; j < m1->col_count; j++)
            {
                m1->grid[i][j] = m2->grid[i - start_index][j];
            }
        }
    }
    else if (mode == MATRIX_CONCAT_MODE_RIGHT)
    {
        unsigned int start_index = m1->col_count;
        m1->col_count += m2->col_count;
        unsigned int i;
        for (i = 0; i < m1->row_count; i++)
        {
            m1->grid[i] = REALLOC(m1->grid[i], m1->col_count, int);
            unsigned int j;
            for (j = start_index; j < m1->col_count; j++)
            {
                m1->grid[i][j] = m2->grid[i][j - start_index];
            }
        }
    }
}

Matrix *
matrix_copy(Matrix matrix)
{
    Matrix *copy = ALLOC(1, Matrix);
    copy->row_count = matrix.row_count;
    copy->col_count = matrix.col_count;
    copy->grid = ALLOC(copy->row_count, int *);

    unsigned int i, j;
    for (i = 0; i < copy->row_count; i++)
    {
        copy->grid[i] = ALLOC(copy->col_count, int);
        for (j = 0; j < copy->col_count; j++)
        {
            copy->grid[i][j] = matrix.grid[i][j];
        }
    }

    return copy;
}

Matrix *
matrix_slice(Matrix matrix, unsigned int c1, unsigned int c2, unsigned int r1, unsigned int r2)
{
    Matrix *sliced_matrix = form_zero_matrix(r2 - r1 + 1, c2 - c1 + 1);

    unsigned int i, j;
    for (i = 0; i < sliced_matrix->row_count; i++)
        for (j = 0; j < sliced_matrix->col_count; j++)
            sliced_matrix->grid[i][j] = matrix.grid[r1 + i][c1 + j];

    return sliced_matrix;
}

void
free_matrix(Matrix *matrix)
{
    unsigned int i;
    for (i = 0; i < matrix->row_count; i++)
    {
        free(matrix->grid[i]);
    }
    free(matrix->grid);
    free(matrix);
}