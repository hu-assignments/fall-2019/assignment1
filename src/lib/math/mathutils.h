//
// Created by MSA on 14/10/2019.
//

#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#include <math.h>

#define MATH_ADD '+'
#define MATH_SUBTRACT '-'
#define MATH_MULTIPLY '*'

int
min(const int *array, unsigned int c);

int
max(const int *array, unsigned int c);

#endif //MATH_UTILS_H
