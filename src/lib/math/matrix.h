/**
 * Created on 12/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef MATH_MATRIX_H
#define MATH_MATRIX_H

#define MATRIX_CONCAT_MODE_RIGHT 0
#define MATRIX_CONCAT_MODE_DOWN 1

typedef struct
{
    unsigned int row_count;
    unsigned int col_count;
    int **grid;
} Matrix;

/**
 * Initializes a zero matrix with the given number of rows and columns.
 * @param row_count M
 * @param col_count N
 * @return A zero matrix.
 */
Matrix *
form_zero_matrix(unsigned int row_count, unsigned int col_count);

void
matrix_fill(Matrix *matrix, int val);

Matrix *
form_matrix(unsigned int row_count, unsigned int col_count, int val);

void
matrix_perform_element_wise_operation(Matrix *m1, Matrix *m2, char operation);

void
matrix_concat(Matrix *m1, Matrix *m2, int mode);

void
matrix_enlarge(Matrix *matrix, unsigned int enlarge_factor_r, unsigned int enlarge_factor_c, int val);

Matrix *
matrix_copy(Matrix matrix);

void
free_matrix(Matrix *matrix);

void
matrix_transpose(Matrix *matrix);

Matrix *
matrix_slice(Matrix matrix, unsigned int c1, unsigned int c2, unsigned int r1, unsigned int r2);

#endif //MATH_MATRIX_H
