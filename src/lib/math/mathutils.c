//
// Created by msa on 13/11/2019.
//

#include "mathutils.h"

int
min(const int *array, unsigned int c)
{
    unsigned int i;
    int min = 0;
    for (i = 0; i < c; i++)
    {
        if (i == 0 || min > array[i])
            min = array[i];
    }

    return min;
}

int
max(const int *array, unsigned int c)
{
    unsigned int i;
    int max = 0;
    for (i = 0; i < c; i++)
    {
        if (i == 0 || max < array[i])
            max = array[i];
    }

    return max;
}
