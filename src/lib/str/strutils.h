//
// Created by MSA on 13/10/2019.
//


#ifndef STRUTILS_H
#define STRUTILS_H

#include <string.h>
#include <stdlib.h>
#include <values.h>
#include <stdbool.h>

#define CRLF "\r\n"
#define LF "\n"

#define CONTAINS(str, sub) (strstr(str, sub) != NULL)
#define EQUALS(str1, str2) ((strcmp(str1, str2) == 0) ? true : false)

/**
 * Counts number of non-overlapping times given substring mentioned in string.
 * @param str
 * @param sub
 * @return
 */
unsigned int
count_substring(char *str, char *sub);

/**
 * Splits the given string with supplied delimiter.
 * Returns a pointer to array of strings.
 *
 * Warning!
 * The supplied string must be dynamically allocated.
 * And it will be modified.
 * @param str The string to be split.
 * @param delimiter The string will be split according to this string.
 * @return An array of strings that are split by the given delimiter.
 */
char **
split(char *str, char *delimiter, unsigned int *length);

/**
 * Removes trailing and leading whitespace characters of the given string.
 * This function modifies the given string, reallocates and returns a new pointer
 * to that string.
 * @param str String to be trimmed.
 * @return A pointer to the trimmed string.
 */
char *
trim(char *str);

/**
 * Replaces all occurrences of given substring in string with given replacement.
 * @param str A dynamically allocated string that will be modified.
 * @param sub Old substring to be replaced.
 * @param replacement New substring.
 * @return The modified string.
 */
char *
replace_str(char *str, char *sub, char *replacement);

bool
is_number(char *str);

char *
unify_lines(char *content);

#endif //STRUTILS_H
