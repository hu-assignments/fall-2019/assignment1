//
// Created by MSA on 26/10/2019.
//

#include <stdio.h>
#include <stdlib.h>
#include "fileutils.h"
#include "../memory/memutils.h"
#include <string.h>

char *
read_file(const char *path)
{
    FILE *f = fopen(path, "rb");
    if (f == NULL)
    {
        char *msg = ALLOC(30 + strlen(path), char);
        sprintf(msg, "Error while opening the file %s", path);
        perror(msg);
        free(msg);
        exit(EXIT_FAILURE);
    }

    fseek(f, 0, SEEK_END);
    long int fsize = ftell(f);
    fseek(f, 0, SEEK_SET); /* To reset seek. */

    char *string = ALLOC(fsize + 1, char);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = '\0';

    return string;
}

char *
safe_read_file(const char *path)
{
    FILE *f = fopen(path, "rb");
    if (f == NULL)
    {
        return NULL;
    }

    fseek(f, 0, SEEK_END);
    long int fsize = ftell(f);
    fseek(f, 0, SEEK_SET); /* To reset seek. */

    char *string = ALLOC(fsize + 1, char);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = '\0';

    return string;
}

