//
// Created by MSA on 13/10/2019.
//

#ifndef FILEUTILS_H
#define FILEUTILS_H

/**
 * A function that returns a char pointer to the content of the file
 * with given name in the working directory, on disk.
 * @param path The path to the file, either absolute or relative.
 * @return The full content of the file.
 */
char *
read_file(const char *path);

char *
safe_read_file(const char *path);

#endif //FILEUTILS_H
