/**
 * Created on 23/10/2019.
 * @author M. Samil Atesoglu
 */

#include <stdio.h>
#include "../lib/str/strutils.h"
#include "../lib/fileio/fileutils.h"
#include "main.h"
#include "matrixman.h"
#include "../lib/memory/memutils.h"

int
main(int argc, char *argv[])
{
    ArgumentSet args;
    args.input_folder_name = argv[1];
    args.command_list_filename = argv[2];
    args.output_filename = argv[3];

    clean_arg_set(&args);

    // Re-routing the standard output to "output.txt".
    freopen(args.output_filename, "w", stdout);

    MatrixRepository matrix_repo;
    matrix_repo.args = args;
    list_construct(&matrix_repo.vector_list, &free_named_matrix);
    list_construct(&matrix_repo.matrix_list, &free_named_matrix);

    char *raw_command_list = unify_lines(safe_read_file(args.command_list_filename));

    begin_execution(&matrix_repo, raw_command_list);

    free(raw_command_list);

    list_free(&(matrix_repo.vector_list));
    list_free(&(matrix_repo.matrix_list));

    return EXIT_SUCCESS;
}

void
clean_arg_set(ArgumentSet *arg_set)
{
    size_t input_folder_name_len = strlen(arg_set->input_folder_name);
    char last_char = arg_set->input_folder_name[input_folder_name_len - 1];
    if (last_char == '/' || last_char == '\\' || last_char == '.')
        arg_set->input_folder_name[input_folder_name_len - 1] = '\0';
}

void
begin_execution(MatrixRepository *matrix_repo, char *raw_commands)
{
    char *line = raw_commands;
    while (line != NULL)
    {
        char *next_line = strchr(line, '\n');
        if (next_line != NULL)
            *next_line = '\0';

        char *command = ALLOC(strlen(line) + 1, char);
        strcpy(command, line);
        command[strlen(line)] = '\0';

        execute_command(matrix_repo, command);

        free(command);

        line = (next_line != NULL) ? (next_line + 1) : NULL;
    }
}