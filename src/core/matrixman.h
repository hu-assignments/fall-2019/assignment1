//
// Created by MSA on 26/10/2019.
//

#ifndef ASSIGNMENT1_MATRIXMAN_H
#define ASSIGNMENT1_MATRIXMAN_H

#include <stdbool.h>
#include "../lib/types/linkedlist.h"
#include "../lib/math/matrix.h"

typedef struct
{
    char *input_folder_name;
    char *command_list_filename;
    char *output_filename;
} ArgumentSet;

typedef struct
{
    Matrix *matrix;
    char *name;
} NamedMatrix;

typedef struct
{
    LinkedList matrix_list;
    LinkedList vector_list;
    ArgumentSet args;
} MatrixRepository;

void
execute_command(MatrixRepository *matrix_repo, char *command);

NamedMatrix *
create_named_matrix(LinkedList *list, char *name, Matrix *matrix);

bool
matrix_already_present(MatrixRepository *repo, char *name);

void
free_named_matrix(void *data);

char *
matrix_to_str(Matrix matrix);

Matrix *
str_to_matrix(char *str);

void
print_matrix(Matrix matrix);

NamedMatrix *
get_named_matrix_by_name(LinkedList *list, char *name);

void
throw_exception();

#endif //ASSIGNMENT1_MATRIXMAN_H
