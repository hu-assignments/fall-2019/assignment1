//
// Created by MSA on 23/10/2019.
//

#ifndef ASSIGNMENT1_MAIN_H
#define ASSIGNMENT1_MAIN_H

#include "matrixman.h"

void
clean_arg_set(ArgumentSet *arg_set);

/**
 * Starts the execution of commands in given input,
 * line by line.
 * @param matrix_repo Matrix repository containing the matrices.
 * @param raw_commands Raw string of commands.
 */
void
begin_execution(MatrixRepository *matrix_repo, char *raw_commands);

#endif //ASSIGNMENT1_MAIN_H
