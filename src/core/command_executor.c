/**
 * Created on 06/11/2019.
 * @author M. Samil Atesoglu
 */

#include "command_executor.h"
#include "../lib/fileio/fileutils.h"
#include "../lib/str/strutils.h"
#include "../lib/memory/memutils.h"
#include "../lib/math/mathutils.h"
#include <stdio.h>

void
execute_veczeros_command(MatrixRepository *matrix_repo, char **arg_list)
{
    char *length_str = arg_list[2];
    if (length_str == NULL) THROW_EXCEPTION()
    if (!is_number(length_str)) THROW_EXCEPTION()
    long int length = strtol(length_str, NULL, 10);

    if (length <= 0) THROW_EXCEPTION()

    if (matrix_already_present(matrix_repo, arg_list[1])) THROW_EXCEPTION()

    char *name = strdup(arg_list[1]);

    NamedMatrix *named_matrix = create_named_matrix(&(matrix_repo->vector_list), name, form_zero_matrix(1, length));

    if (named_matrix)
    {
        printf("created vector %s %ld\n", name, length);
        print_matrix(*(named_matrix->matrix));
    }
}

void
execute_matzeros_command(MatrixRepository *matrix_repo, char **arg_list)
{
    char *row_count_str = arg_list[2];
    char *col_count_str = arg_list[3];

    if (row_count_str == NULL || col_count_str == NULL) THROW_EXCEPTION()

    if (!is_number(row_count_str) || !is_number(col_count_str)) THROW_EXCEPTION()

    long int row_count = strtol(row_count_str, NULL, 10);
    long int col_count = strtol(col_count_str, NULL, 10);

    if (row_count <= 0 || col_count <= 0) THROW_EXCEPTION()

    if (matrix_already_present(matrix_repo, arg_list[1])) THROW_EXCEPTION()

    char *name = strdup(arg_list[1]);

    NamedMatrix *named_matrix = create_named_matrix(&(matrix_repo->matrix_list), name, form_zero_matrix(row_count, col_count));

    if (named_matrix != NULL)
    {
        printf("created matrix %s %ld %ld\n", name, row_count, col_count);
        print_matrix(*(named_matrix->matrix));
    }
    else THROW_EXCEPTION(free(name);)
}

void
execute_read_command(MatrixRepository *matrix_repo, char **arg_list, int mode)
{
    char *filename = arg_list[1];
    if (filename == NULL) THROW_EXCEPTION()
    char *filename_cpy = strdup(filename);

    char **name_extension = split(filename_cpy, ".", NULL);
    char *name = strdup(name_extension[0]);
    free(name_extension);
    free(filename_cpy);

    if (matrix_already_present(matrix_repo, name)) THROW_EXCEPTION(free(name);)

    size_t path_len = strlen(filename) + strlen(matrix_repo->args.input_folder_name) + 1;
    char *path = ALLOC(path_len + 1, char);
    snprintf(path, path_len + 1, "%s/%s", matrix_repo->args.input_folder_name, filename);

    char *str_matrix = unify_lines(safe_read_file(path));
    free(path);

    if (str_matrix == NULL) THROW_EXCEPTION(free(name);)

    Matrix *matrix = str_to_matrix(str_matrix);
    free(str_matrix);

    if (matrix == NULL) THROW_EXCEPTION(
        free(name);
    )

    NamedMatrix *named_matrix = ALLOC(1, NamedMatrix);
    named_matrix->matrix = matrix;
    named_matrix->name = name;

    switch (mode)
    {
        case MODE_VEC:
            list_push(&(matrix_repo->vector_list), named_matrix);
            printf("read vector %s %d\n", filename, named_matrix->matrix->col_count);
            break;
        case MODE_MAT:
            list_push(&(matrix_repo->matrix_list), named_matrix);
            printf("read matrix %s %d %d\n", filename, named_matrix->matrix->row_count,
                   named_matrix->matrix->col_count);
            break;
        default:
            break;
    }
    print_matrix(*(named_matrix->matrix));
}

void
execute_stack_command(MatrixRepository *matrix_repo, char **arg_list, int mode)
{
    char *name1 = arg_list[1];
    char *name2 = arg_list[2];

    char *direction;

    if (mode == MODE_VEC)
    {
        direction = arg_list[3];

        NamedMatrix *v1 = get_named_matrix_by_name(&(matrix_repo->vector_list), name1);
        NamedMatrix *v2 = get_named_matrix_by_name(&(matrix_repo->vector_list), name2);

        if (v1 == NULL || v2 == NULL) THROW_EXCEPTION()
        if (v1->matrix->col_count != v2->matrix->col_count) THROW_EXCEPTION()

        char *new_name = strdup(arg_list[4]);

        if (matrix_already_present(matrix_repo, new_name)) THROW_EXCEPTION(free(new_name);)

        Matrix *new_matrix = matrix_copy(*v1->matrix);
        NamedMatrix *new_named_matrix =
            create_named_matrix(&(matrix_repo->matrix_list), new_name, new_matrix);
        new_named_matrix->matrix = new_matrix;

        if (EQUALS(direction, "row"))
        {
            matrix_concat(new_named_matrix->matrix, v2->matrix, MATRIX_CONCAT_MODE_DOWN);
        }
        else if (EQUALS(direction, "column"))
        {
            matrix_transpose(new_named_matrix->matrix);
            Matrix *v2_cpy = matrix_copy(*v2->matrix);
            matrix_transpose(v2_cpy);
            matrix_concat(new_named_matrix->matrix, v2_cpy, MATRIX_CONCAT_MODE_RIGHT);
            free_matrix(v2_cpy);
        }
        else THROW_EXCEPTION()

        printf("vectors concatenated %s %d %d\n", new_name, new_named_matrix->matrix->row_count, new_named_matrix->matrix->col_count);
        print_matrix(*(new_named_matrix->matrix));
    }
    else if (mode == MODE_MAT || mode == MODE_MV)
    {
        NamedMatrix *m1 = get_named_matrix_by_name(&(matrix_repo->matrix_list), name1);
        NamedMatrix *m2 = get_named_matrix_by_name(((mode == MODE_MV) ? &(matrix_repo->vector_list) : &(matrix_repo->matrix_list)), name2);

        if (m1 == NULL || m2 == NULL) THROW_EXCEPTION()

        direction = arg_list[3];

        if (EQUALS(direction, "r"))
        {
            if (mode == MODE_MAT)
            {
                if (m1->matrix->row_count != m2->matrix->row_count) THROW_EXCEPTION()
                Matrix *m_cpy = matrix_copy(*m2->matrix);
                matrix_concat(m1->matrix, m_cpy, MATRIX_CONCAT_MODE_RIGHT);
                free_matrix(m_cpy);
            }
            else // MODE_MV
            {
                if (m1->matrix->row_count != m2->matrix->col_count) THROW_EXCEPTION()
                Matrix *m_cpy = matrix_copy(*m2->matrix);
                matrix_transpose(m_cpy);
                matrix_concat(m1->matrix, m_cpy, MATRIX_CONCAT_MODE_RIGHT);
                free_matrix(m_cpy);
            }
        }
        else if (EQUALS(direction, "d"))
        {
            if (m1->matrix->col_count != m2->matrix->col_count) THROW_EXCEPTION()
            matrix_concat(m1->matrix, m2->matrix, MATRIX_CONCAT_MODE_DOWN);
        }
        else THROW_EXCEPTION()

        printf(mode == MODE_MV ? "matrix and vector concatenated %s %d %d\n" : "matrices concatenated %s %d %d\n",
               name1,
               m1->matrix->row_count,
               m1->matrix->col_count);

        print_matrix(*(m1->matrix));
    }
}

void
execute_math_command(MatrixRepository *matrix_repo, char **arg_list, int mode)
{
    char *name1 = arg_list[1];
    char *name2 = arg_list[2];

    NamedMatrix *m1 = get_named_matrix_by_name(&(matrix_repo->matrix_list), name1);
    NamedMatrix *m2 = get_named_matrix_by_name(&(matrix_repo->matrix_list), name2);

    if (m1 == NULL || m2 == NULL) THROW_EXCEPTION()

    if ((m1->matrix->row_count != m2->matrix->row_count) && (m1->matrix->col_count != m2->matrix->col_count)) THROW_EXCEPTION()

    switch (mode)
    {
        case MODE_ADD:
            matrix_perform_element_wise_operation(m1->matrix, m2->matrix, MATH_ADD);
            printf("add %s %s\n", name1, name2);
            break;
        case MODE_SUBTRACT:
            matrix_perform_element_wise_operation(m1->matrix, m2->matrix, MATH_SUBTRACT);
            printf("subtract %s %s\n", name1, name2);
            break;
        case MODE_MULTIPLY:
            matrix_perform_element_wise_operation(m1->matrix, m2->matrix, MATH_MULTIPLY);
            printf("multiply %s %s\n", name1, name2);
            break;
        default:
            break;
    }

    print_matrix(*(m1->matrix));
}

void
execute_slice_command(MatrixRepository *matrix_repo, char **arg_list, int mode)
{
    char *name = arg_list[1];
    NamedMatrix *named_matrix = get_named_matrix_by_name(mode == MODE_SLICE_VEC ? &(matrix_repo->vector_list) : &(matrix_repo->matrix_list), name);

    if (named_matrix == NULL) THROW_EXCEPTION()

    if (mode == MODE_SLICE_MAT)
    {
        if (!is_number(arg_list[2]) || !is_number(arg_list[3]) || !is_number(arg_list[4]) || !is_number(arg_list[5])) THROW_EXCEPTION()
        unsigned int y1 = strtol(arg_list[2], NULL, 10);
        unsigned int y2 = strtol(arg_list[3], NULL, 10) - 1;
        unsigned int x1 = strtol(arg_list[4], NULL, 10);
        unsigned int x2 = strtol(arg_list[5], NULL, 10) - 1;

        if (y1 < 0 || y2 < 0 || x1 < 0 || x2 < 0) THROW_EXCEPTION()
        if (y1 > y2 || x1 > x2) THROW_EXCEPTION()
        if (y2 + 1 == y1 || x2 + 1 == x1) THROW_EXCEPTION()
        if (y2 > named_matrix->matrix->col_count - 1 || x2 > named_matrix->matrix->row_count - 1) THROW_EXCEPTION()

        char *new_name = strdup(arg_list[6]);

        if (matrix_already_present(matrix_repo, new_name)) THROW_EXCEPTION(free(new_name);)

        Matrix *sliced_matrix = matrix_slice(*(named_matrix->matrix), y1, y2, x1, x2);

        NamedMatrix *new_named_matrix = create_named_matrix(&(matrix_repo->matrix_list), new_name, sliced_matrix);
        printf("matrix sliced %s %d %d\n", new_named_matrix->name, new_named_matrix->matrix->row_count, new_named_matrix->matrix->col_count);
        print_matrix(*(new_named_matrix->matrix));
    }
    else
    {
        char *start_i_str = arg_list[(mode == MODE_SLICE_VEC) ? 2 : 3];
        char *stop_i_str = arg_list[(mode == MODE_SLICE_VEC) ? 3 : 4];
        if (!is_number(stop_i_str) || !is_number(start_i_str)) THROW_EXCEPTION()

        unsigned int start_index = strtol(start_i_str, NULL, 10);
        unsigned int stop_index = strtol(stop_i_str, NULL, 10) - 1;

        if (start_index < 0 || stop_index < 0 || stop_index < start_index) THROW_EXCEPTION()
        if (stop_index > named_matrix->matrix->col_count) THROW_EXCEPTION()

        char *new_name = arg_list[(mode == MODE_SLICE_VEC) ? 4 : 5];

        if (matrix_already_present(matrix_repo, new_name)) THROW_EXCEPTION()

        if (mode == MODE_SLICE_VEC)
        {
            Matrix *sliced_vector = matrix_slice(*named_matrix->matrix, start_index, stop_index, 0, 0);

            NamedMatrix *new_named_vector = create_named_matrix(&(matrix_repo->vector_list), strdup(new_name), sliced_vector);

            printf("vector sliced %s %d\n", new_named_vector->name, new_named_vector->matrix->col_count);
            print_matrix(*(new_named_vector->matrix));
        }
        else
        {
            if (!is_number(arg_list[2])) THROW_EXCEPTION()
            unsigned int index = strtol(arg_list[2], NULL, 10);
            if (index < 0) THROW_EXCEPTION()

            NamedMatrix *new_named_vector;

            if (mode == MODE_SLICE_MAT_C)
            {
                if (index > named_matrix->matrix->col_count - 1) THROW_EXCEPTION()

                Matrix *sliced_matrix = matrix_slice(*named_matrix->matrix, index, index, start_index, stop_index);

                new_named_vector = create_named_matrix(&(matrix_repo->vector_list), strdup(new_name), sliced_matrix);

                matrix_transpose(new_named_vector->matrix);
            }
            else // (mode == MODE_SLICE_MAT_R)
            {
                if (index > named_matrix->matrix->row_count - 1) THROW_EXCEPTION()

                Matrix *sliced_matrix = matrix_slice(*named_matrix->matrix, start_index, stop_index, index, index);

                new_named_vector = create_named_matrix(&(matrix_repo->vector_list), strdup(new_name), sliced_matrix);
            }

            printf("vector sliced %s %d\n", new_named_vector->name, new_named_vector->matrix->col_count);
            print_matrix(*(new_named_vector->matrix));
        }
    }
}

void
execute_pad_command(MatrixRepository *matrix_repo, char **arg_list, int mode)
{
    if (!is_number(arg_list[2]) || !is_number(arg_list[3])) THROW_EXCEPTION()

    int enlarge_factor_r = (int) strtol(arg_list[2], NULL, 10);
    int enlarge_factor_c = (int) strtol(arg_list[3], NULL, 10);

    if (enlarge_factor_r < 0 || enlarge_factor_c < 0) THROW_EXCEPTION()

    NamedMatrix *named_matrix = get_named_matrix_by_name(&(matrix_repo->matrix_list), arg_list[1]);
    if (named_matrix == NULL) THROW_EXCEPTION()

    if (mode == MODE_PAD_VAL)
    {
        if (!is_number(arg_list[4])) THROW_EXCEPTION()
        int val = (int) strtol(arg_list[4], NULL, 10);

        matrix_enlarge(named_matrix->matrix, enlarge_factor_r, enlarge_factor_c, val);

        printf("matrix paded %s %d %d\n", named_matrix->name, named_matrix->matrix->row_count, named_matrix->matrix->col_count);
        print_matrix(*(named_matrix->matrix));
    }
    else if (mode == MODE_PAD)
    {
        char *pad_mode_str = arg_list[4];

        Matrix *matrix = named_matrix->matrix;

        unsigned int old_row_count = matrix->row_count;
        unsigned int old_col_count = matrix->col_count;

        int pad_mode;
        if (EQUALS(pad_mode_str, "maximum"))
            pad_mode = MODE_PAD_MAX;
        else if (EQUALS(pad_mode_str, "minimum"))
            pad_mode = MODE_PAD_MIN;
        else THROW_EXCEPTION()

        matrix_enlarge(matrix, enlarge_factor_r, enlarge_factor_c, 0);

        unsigned int i, j;
        for (i = 0; i < old_row_count; i++)
        {
            if (i < old_row_count)
            {
                int val = pad_mode == MODE_PAD_MAX ? max(matrix->grid[i], old_col_count) : min(matrix->grid[i], old_col_count);
                for (j = old_col_count; j < matrix->col_count; j++)
                {
                    matrix->grid[i][j] = val;
                }
            }
        }

        matrix_transpose(matrix);

        for (i = 0; i < matrix->row_count; i++)
        {
            int val = pad_mode == MODE_PAD_MAX ? max(matrix->grid[i], old_row_count) : min(matrix->grid[i], old_row_count);
            for (j = old_row_count; j < matrix->col_count; j++)
            {
                matrix->grid[i][j] = val;
            }
        }

        matrix_transpose(matrix);

        printf("matrix paded %s %d %d\n", named_matrix->name, named_matrix->matrix->row_count, named_matrix->matrix->col_count);
        print_matrix(*(named_matrix->matrix));
    }
}

void
throw_exception()
{
    printf("error\n");
}