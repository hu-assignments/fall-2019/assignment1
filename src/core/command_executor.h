/**
 * Created on 06/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef ASSIGNMENT1_COMMAND_EXECUTOR_H
#define ASSIGNMENT1_COMMAND_EXECUTOR_H

#include "matrixman.h"

#define MODE_VEC 10
#define MODE_MAT 11
#define MODE_MV 12
#define MODE_ADD 0
#define MODE_SUBTRACT 1
#define MODE_MULTIPLY 2
#define MODE_SLICE_VEC 3
#define MODE_SLICE_MAT 4
#define MODE_SLICE_MAT_R 5
#define MODE_SLICE_MAT_C 6
#define MODE_PAD_VAL 7
#define MODE_PAD 8
#define MODE_PAD_MAX 9
#define MODE_PAD_MIN 10

#define THROW_EXCEPTION(free) { throw_exception(); free return; }

void
execute_veczeros_command(MatrixRepository *matrix_repo, char **arg_list);

void
execute_matzeros_command(MatrixRepository *matrix_repo, char **arg_list);

void
execute_read_command(MatrixRepository *matrix_repo, char **arg_list, int mode);

void
execute_stack_command(MatrixRepository *matrix_repo, char **arg_list, int mode);

void
execute_pad_command(MatrixRepository *matrix_repo, char **arg_list, int mode);

void
execute_slice_command(MatrixRepository *matrix_repo, char **arg_list, int mode);

void
execute_math_command(MatrixRepository *matrix_repo, char **arg_list, int mode);

void
throw_exception();

#endif //ASSIGNMENT1_COMMAND_EXECUTOR_H
