/**
 * Created on 26/10/2019.
 * @author M. Samil Atesoglu
 */

#include "matrixman.h"
#include "../lib/memory/memutils.h"
#include "../lib/str/strutils.h"
#include "command_executor.h"
#include "../lib/math/mathutils.h"
#include <stdio.h>

void
execute_command(MatrixRepository *matrix_repo, char *command)
{
    if (strlen(command) > 0)
    {
        char *command_cpy = ALLOC(strlen(command) + 1, char);
        strcpy(command_cpy, command);

        unsigned int length;
        char **splt = split(command_cpy, " ", &length);
        char *base_command = splt[0];

        if (EQUALS(base_command, "veczeros"))
        {
            if (length != 3)
                throw_exception();
            else execute_veczeros_command(matrix_repo, splt);
        }
        else if (EQUALS(base_command, "matzeros"))
        {
            if (length != 4)
                throw_exception();
            else execute_matzeros_command(matrix_repo, splt);
        }
        else if (EQUALS(base_command, "vecread"))
        {
            if (length != 2)
                throw_exception();
            else execute_read_command(matrix_repo, splt, MODE_VEC);
        }
        else if (EQUALS(base_command, "matread"))
        {
            if (length != 2)
                throw_exception();
            else execute_read_command(matrix_repo, splt, MODE_MAT);
        }
        else if (EQUALS(base_command, "vecstack"))
        {
            if (length != 5)
                throw_exception();
            else execute_stack_command(matrix_repo, splt, MODE_VEC);
        }
        else if (EQUALS(base_command, "matstack"))
        {
            if (length != 4)
                throw_exception();
            else execute_stack_command(matrix_repo, splt, MODE_MAT);
        }
        else if (EQUALS(base_command, "mvstack"))
        {
            if (length != 4)
                throw_exception();
            else execute_stack_command(matrix_repo, splt, MODE_MV);
        }
        else if (EQUALS(base_command, "pad"))
        {
            if (length != 5)
                throw_exception();
            else execute_pad_command(matrix_repo, splt, MODE_PAD);
        }
        else if (EQUALS(base_command, "padval"))
        {
            if (length != 5)
                throw_exception();
            else execute_pad_command(matrix_repo, splt, MODE_PAD_VAL);
        }
        else if (EQUALS(base_command, "vecslice"))
        {
            if (length != 5)
                throw_exception();
            else execute_slice_command(matrix_repo, splt, MODE_SLICE_VEC);
        }
        else if (EQUALS(base_command, "matslicecol"))
        {
            if (length != 6)
                throw_exception();
            else execute_slice_command(matrix_repo, splt, MODE_SLICE_MAT_C);
        }
        else if (EQUALS(base_command, "matslicerow"))
        {
            if (length != 6)
                throw_exception();
            else execute_slice_command(matrix_repo, splt, MODE_SLICE_MAT_R);
        }
        else if (EQUALS(base_command, "matslice"))
        {
            if (length != 7)
                throw_exception();
            else execute_slice_command(matrix_repo, splt, MODE_SLICE_MAT);
        }
        else if (EQUALS(base_command, "add"))
        {
            if (length != 3)
                throw_exception();
            else execute_math_command(matrix_repo, splt, MODE_ADD);
        }
        else if (EQUALS(base_command, "multiply"))
        {
            if (length != 3)
                throw_exception();
            else execute_math_command(matrix_repo, splt, MODE_MULTIPLY);
        }
        else if (EQUALS(base_command, "subtract"))
        {
            if (length != 3)
                throw_exception();
            execute_math_command(matrix_repo, splt, MODE_SUBTRACT);
        }
        else
            throw_exception();

        free(command_cpy);
        free(splt);
    }
}

NamedMatrix *
create_named_matrix(LinkedList *list, char *name, Matrix *matrix)
{
    NamedMatrix *named_matrix = ALLOC(1, NamedMatrix);
    named_matrix->name = name;
    named_matrix->matrix = matrix;
    list_push(list, named_matrix);
    return named_matrix;
}

bool
matrix_already_present(MatrixRepository *repo, char *name)
{
    if (get_named_matrix_by_name(&(repo->matrix_list), name))
        return true;

    if (get_named_matrix_by_name(&(repo->vector_list), name))
        return true;

    return false;
}

void
free_named_matrix(void *data)
{
    NamedMatrix *named_matrix = ((NamedMatrix *) data);
    free(named_matrix->name);
    free_matrix(named_matrix->matrix);
    free(named_matrix);
}

Matrix *
str_to_matrix(char *str)
{
    if (str[strlen(str) - 1] == '\n')
    {
        str[strlen(str) - 1] = '\0';
    }

    unsigned int row_count = count_substring(str, "\n") + 1;
    unsigned int column_count = count_substring(str, " ") / row_count + 1;

    int **raw_matrix = ALLOC(row_count, int*);

    char **str_rows = split(str, "\n",NULL);
    unsigned int i, j;
    for (i = 0; i < row_count; i++)
    {
        raw_matrix[i] = ALLOC(column_count, int);
        char **str_columns = split(str_rows[i], " ",NULL);
        for (j = 0; j < column_count; j++)
        {
            char *end_ptr;

            int val = (int) strtol(str_columns[j], &end_ptr, 10);

            if (end_ptr == str_columns[j])
                return NULL;

            raw_matrix[i][j] = val;
        }
        free(str_columns);
    }
    free(str_rows);

    Matrix *matrix = ALLOC(1, Matrix);
    matrix->row_count = row_count;
    matrix->col_count = column_count;
    matrix->grid = raw_matrix;

    return matrix;
}

char *
matrix_to_str(Matrix matrix)
{
    unsigned int size = 1;
    char *str = ALLOC(size, char);
    unsigned int i, j;
    for (i = 0; i < matrix.row_count; i++)
    {
        unsigned int row_str_len = 1;
        for (j = 0; j < matrix.col_count; j++)
        {
            int val = matrix.grid[i][j];
            unsigned int number_of_digits = (unsigned int) (log10(abs(val))) + 1;
            unsigned int length_number_str = (val >= 0) ? number_of_digits : number_of_digits + 1;
            char *val_str = ALLOC(length_number_str + 2, char);
            char *format_str = (j == matrix.col_count - 1) ? "%d\n" : "%d ";
            snprintf(val_str, length_number_str + 2, format_str, val);
            row_str_len += strlen(val_str);
            str = REALLOC(str, size + row_str_len, char);
            strcat(str, val_str);
            free(val_str);
        }
        size += row_str_len;
    }
    return str;
}

void
print_matrix(Matrix matrix)
{
    char *matrix_str = matrix_to_str(matrix);
    printf("%s", matrix_str);
    free(matrix_str);
}

NamedMatrix *
get_named_matrix_by_name(LinkedList *list, char *name)
{
    unsigned int length = list_length(list);
    unsigned int i;
    for (i = 0; i < length; i++)
    {
        NamedMatrix *current_matrix = LIST_GET(list, i, NamedMatrix *);
        if (EQUALS(current_matrix->name, name))
        {
            return current_matrix;
        }
    }

    return NULL;
}